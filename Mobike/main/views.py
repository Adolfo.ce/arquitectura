from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from .forms import UserRegisterForm
from django.contrib.auth.decorators import login_required, permission_required
# Create your views here.

def homepage(request):
    return render(request, 'main/inicio.html',{})

def registro(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            messages.success(request, f'Usuario {username} creado')
            form.save()
            return redirect('main:homepage')
    else:
        form = UserRegisterForm()

    context = {'form': form}
    return render(request, 'main/registro.html',context)
    

def logout_request(request):
    logout(request)
    messages.info(request, "saliste exitosamente")
    return redirect("main:homepage")

def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            usuario = form.cleaned_data.get('username')
            contraseña = form.cleaned_data.get('password')
            user = authenticate(username= usuario, password = contraseña)
            if user is not None:
                login(request,user)
                messages.info(request, f"estas logeado como {usuario}")
                return redirect("main:homepage")
            else:
                messages.error(request, "Usuario o contraseña equivocada")
        else:
            messages.error(request, "Usuario o contraseña equivocada")

    form = AuthenticationForm()
    return render(request, "main/login.html", {"form": form})



def cliente(request):
    return render (request, 'main/cliente.html', {})

@permission_required('main.add_admin_model1')
@login_required
def view_admin(request):
    return render (request, 'main/view_admin.html',{})

@permission_required('main.add_funcionario_model')
def view_funcio(request):
    return render (request, 'main/view_funcio.html',{})