from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class UserRegisterForm(UserCreationForm):
    username = forms.CharField(label='Nombre', max_length=200)
    apellido = forms.CharField(label='Apellido', max_length=200)
    email = forms.EmailField()
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirma Contraseña', widget=forms.PasswordInput)
    telefono = forms.CharField(label='telefono', max_length=12)
    tarjeta_bco = forms.CharField(label='Tarjeta de Credito', max_length=12)
    rut = forms.CharField(label='Rut', max_length=10)


    class Meta:
        model = User
        fields = ['username', 'apellido' , 'email', 'password1', 'password2',
                  'telefono', 'tarjeta_bco', 'rut']
        help_text = {k:"" for k in fields}
        helptext= {k:"" for k in fields}