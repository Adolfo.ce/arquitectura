from django.db import models

# Create your models here.

class Cliente(models.Model):
    Nombre= models.CharField(max_length=200)
    Apellido=models.CharField(max_length=200)
    Contraseña= models.CharField(max_length=200)
    Correo= models.CharField(max_length=200)
    Telefono_Contacto= models.CharField(max_length=10)
    Tarjeta_Bancaria= models.CharField(max_length=12)
    Run= models.CharField(max_length=10)



class Funcionario_model(models.Model):
    funcionario = models.CharField(max_length=20)
    contenido = models.CharField(max_length=500)
    fecha = models.DateField()
    
class Admin_model1(models.Model):
    adm_nombre = models.CharField(max_length=200)
    adm_apellido = models.CharField(max_length=200)
    adm_contraseña = models.CharField(max_length=200)
    adm_correo = models.CharField(max_length=200)
    adm_telefono = models.CharField(max_length=10)
    adm_bco = models.CharField(max_length=16)
    adm_run = models.CharField(max_length=10)