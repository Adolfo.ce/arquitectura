from django.urls import path, include
from . import views

app_name='main'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('registro/', views.registro, name='registro'),
    path('logout/', views.logout_request, name= 'logout'),
    path('login/', views.login_request, name="login"),
    path('cliente/', views.cliente, name= 'cliente'),
    path('view_admin/', views.view_admin, name= 'view_admin'),
    path('view_funcio', views.view_funcio, name= 'view_funcio'),
]
